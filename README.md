# Emulator "Mutsu" for FM-7/FM77AV/FM77AV40

![image](https://www.fujitsu.com/global/imagesgig5/3_fm-7_tcm100-5414489_tcm100-2750236-32.jpg)

## Introduction
It is an emulator for the Fujitsu FM-7 (8bit) series computers. Some of the features are :

- Cross-platform (Windows/Linux/macOS).
- Can choose the graphical user interface and control.
- Powerful debugger
- Cooperation with 2D retro map tool.
- The goal is to emulate the FM77AV40. Almost 99.9% of the FM-7 series software runs on FM77AV. The minimum goal is to support FM77AV.


## Information
The aim of the project is to be able to easily compile the Mutsu core. It needs source files from another repository. Here, we have everything we need to do this compilation from a binary creation package.

## Authors and acknowledgment
Thanks to Soji Yamakawa (captainys) for offering us an excellent emulator.

Source : https://github.com/captainys

## License
BSD 3-Clause License
